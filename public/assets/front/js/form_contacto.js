$(function(){
	$('.formulario').jqTransform({imgPath:'jqtransformplugin/img/'});

	var frm=$('#frm_contact');

	$('.refresh a').click(function(){
		captcha_refresh();
	});

    frm.submit(function(e){
        e.preventDefault();

        if($('#contact_id').val()==''){
        	alert('Debe clasificar su tipo de consulta.');
        	return;
        }

        if(!$('#checkin').is(':checked')){
        	alert('Debe aceptar los términos y condiciones.');
        	return;
        }

        frm.hide();
        frm.after('<div id="loading">Enviando los datos...</div>');
		$(document).scrollTop(0);

        $.ajax({
            type: "POST",
            url : URL_ROOT + "/ajax/form/post",
            data : frm.serialize(),
            success : function(data){
	            $('#loading').remove();
	            console.log(data.resp);
	            if(data.resp==1){
	                $('#frm_msg').show();
	            }else{
					captcha_refresh();
	                frm.show();
	                alert(data.msg);
	            }
			},
			error: function(data) {
	            $('#loading').remove();
				captcha_refresh();
                frm.show();
			}
        },"html");
    });

	function captcha_refresh(){
		$.ajax({
		url: URL_ROOT + "/ajax/form/refresh_captcha",
		type: 'get',
			success: function(data) {
				$('.captcha').html(data);
				$('#captcha').val('');
			}
        },"html");
	}

});

$(document).ajaxError(function( event, request, settings, exception) {
	alert('Ha ocurrido un error interno en la aplicaci\xf3n. Por favor, int\xe9ntelo mas tarde.');
	console.log("url: " + settings.url);
	console.log("error: " + exception);
	console.log("data: " + request.responseText);
});
