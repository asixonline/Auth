<?php
use \App\Util\SEO;

$list = $page->children;
?>

<h2>{{ $parents[0]->title }}</h2>
<h1>{{ $page->title }}</h1>

@foreach($list as $item)
<?php
	$url=SEO::url_article($item);
	$imagen=\App\Util\XMLParser::getValue($item->media, 'imagen');
	$date =strtotime($item->date);
	$datef=date('d', $date).' de '.\App\Util\DateFormat::get_Mes(date('m', $date)).' de '.date('Y', $date);
?>
<div class="noticias_interna">
	<a href="{{ $url }}" class="full"></a>
	<div class="imagen">
		<img src="{{ asset('/userfiles/'.$imagen) }}">
	</div>
	<div class="texto_noticia">
		<span>{{ $datef }}</span>
		<span class="noticia_titulo">{{ $item->title }}</span>
		{!! $item->resumen !!}
	</div>
	<div class="ico_noticia">
		<a href="{{ $url }}"><img src="{{ asset('/assets/front/images/ico_mas.png') }}"></a>
	</div>
	<div class="clear"></div>
</div>
@endforeach
