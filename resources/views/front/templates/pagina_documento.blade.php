<?php
$tabs=$page->children;
?>
<div class="superior">
	<h2>{{ $parents[0]->title }}</h2>
	<h1>{{ $page->title }}</h1>
</div>

<div class="texto100">
	{!! $page->description !!}
</div>

@if(count($tabs)>0)
<ul class="lista_consejo_de_usuario">
	@foreach($tabs as $tab)
	<li>
		<div class="word azul">
			{{ $tab->title }}
			<div class="solapa"></div>
		</div>
	</li>
	@endforeach
</ul>
<ul class="desplegable">
	@foreach($tabs as $tab)
	<?php
		$block=$tab->children;
		$view=$tab->schema->front_view;
	?>
	<li class="lista_desplegable">
		{!! $tab->resumen !!}
	@if($view=='bloque_documentos')
		<ul class="lista_descargas">
			@foreach($block as $doc)
			<?php
				$documento=\App\Util\XMLParser::getValue($doc->media, 'documento');
			?>
			<li>
				{{ $doc->title }}
				@if(!empty($documento))
				<div class="btn_descargar">
					<a href="{{ asset('/userfiles/'.$documento) }}" target="_blank" class="full"></a>
					Descargar
					<div class="ico">
						<img src="{{ asset('/assets/front/images/consejo-usuarios-descargas-interna.png') }}">
					</div>
				</div>
				@endif
			</li>
			@endforeach
		</ul>
	@endif
	@if($view=='bloque_miembros')
		<ul class="lista_desplegable">
			<div class="contendor_tabla">
			  <table border="0" cellspacing="0" cellpadding="0" style="width: 761px;">
				<thead>
					<tr>
						<th scope="col">N.</th>
						<th scope="col">Miembro</th>
						<th scope="col">Institución</th>
						<th scope="col">Cargo</th>
					</tr>
				</thead>
			<?php $i=1; ?>
			@foreach($block as $obj)
				<tr>
					<td>{{ $i++ }}</td>
					<td>{{ $obj->title }}</td>
					<td>{{ $obj->subtitle }}</td>
					<td>{{ $obj->subtitle2 }}</td>
				</tr>
			@endforeach
			  </table>
			</div>
		</ul>
	@endif

	</li>
	@endforeach
</ul>
@endif

<script type="text/javascript">
$(function(){
	lista_consejo_de_usuario();
});
</script>