<?php
$form=\App\CrmForm::select()->where('alias', 'contacto')->first();
$group=\App\CmsParameterGroup::select()->where('alias', 'contacto')->first();
$contacts = [null => 'Seleccionar'] + $group->parameters->lists('name', 'id')->toarray();
?>
<h2>{{ $parents[0]->title }}</h2>
<h1>{{ $page->title }}</h1>

<div class="formulario">
    {!! Form::open(['id'=>'frm_contact', 'url' => 'ajax/form/post', 'method'=>'POST']) !!}
      {!! Form::hidden('form_id', $form->id) !!}
		<div class="area_blanca no-padding">
			<div class="texto100">
				{!! $page->description !!}
			</div>

			<div class="campo">
				<div class="txt">
					Nombres*
				</div>
				<input type="text" name="first_name" id="first_name" required="true">
			</div>
			<div class="campo right">
				<div class="txt">
					Apellidos*
				</div>
				<input type="text" name="last_name" id="last_name" required="true">
			</div>
			<div class="campo">
				<div class="txt">
					DNI*
				</div>
				<input type="num" name="dni" id="dni" required="true">
			</div>
			<div class="campo right">
				<div class="txt">
					Dirección*
				</div>
				<input type="text" name="address" id="address" required="true">
			</div>
			<div class="campo">
				<div class="txt">
					Teléfonos*
				</div>
				<input type="telf" name="phone" id="phone" required="true">
			</div>
			<div class="campo right">
				<div class="txt">
					Email:*
				</div>
				<input type="email" name="email" id="email" required="true">
			</div>							
			<div class="clear"></div>
		</div>				
		<div class="caja_gris">
			
			<div class="txt">
				Clasifique su Consulta*:
			</div>
			<div class="campo_selecto padding-selecto">
				{!! Form::select('contact_id', $contacts, null, ['id'=>'contact_id', 'class'=>'form-control']) !!}
			</div>
			<div class="txt">
				Descripción de la consulta*:
			</div>
			<textarea name="comment" id="comment" required="true"></textarea>
			<div class="ayuda hidden top-help">
				<div class="triangle"></div>
				Usted puede ingresar su consulta y lo estaremos contactando en la brevedad de lo posible
			</div>
		</div>
		<div class="area_blanca">
			<div class="captcha">
				{!! Captcha::img('simple'); !!}
			</div>
			<div class="refresh" style="display: inline-block; padding-left: 10px; height: 63px; width: 200px;">
				<a href="javascript:;" style="padding-top: 30px; display: block;">Otra imagen</a>
			</div>
			<div class="clear"></div>
			<div class="campo">
				<div class="txt">
					Escribe el texto de la imagen*
				</div>
				<input type="text" name="captcha" id="captcha" required="true">
			</div>
			<div class="clear"></div>
		</div>	
		<div class="caja_gris">
			<input type="checkbox" name="checkin" id="checkin" value="1">
			<div class="acepto">
				Acepto los <a href="#">términos y condiciones*</a>
			</div>
		</div>
		<div class="clear"></div>
		<div class="area_botones">
			<button type="reset" class="btn_borrar" style="border: 0px; padding-top: 0; font-size: 12px;">Borrar</button>
			<button type="submit" class="btn_enviar" style="border: 0px; padding-top: 0; font-size: 12px;">Enviar</button>
			<div class="clear"></div>
			<div class="texto100">
				*Campos obligatorios
			</div>
		</div>
    {!! Form::close() !!}

	<div id="frm_msg" style="display:none">
	  <h3 style="font-weight: bold; font-size: large; margin-bottom:10px">Sus datos se enviaron exitosamente.</h3>
	  <p>Gracias por registrarse, pronto estaremos en contacto.</p>
	</div>

</div>

<script src="{{ asset('/assets/front/js/form_contacto.js') }}"></script>
<script>
var URL_ROOT="{{ url('/') }}";
</script>
