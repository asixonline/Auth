<?php
	$imagen=\App\Util\XMLParser::getValue($page->media, 'imagen');
	$date =strtotime($page->date);
	$datef=date('d', $date).' de '.\App\Util\DateFormat::get_Mes(date('m', $date)).' de '.date('Y', $date);
?>
<h2>{{ $parents[0]->title }}</h2>
<h1>{{ $parents[1]->title }}</h1>

<div class="btn_regresar_noticias">
	<a href="{{ asset('/'.$parents[1]->slug.'.html') }}" class="full"></a>
	regresar
</div>
<div class="noticias_interna noticia_detalle">
	<div class="imagen">
		<img src="{{ asset('/userfiles/'.$imagen) }}">
	</div>
	<div class="texto_noticia">
		<span class="noticia_fecha">{{ $datef }}</span>
		<span class="noticia_titulo">{{ $page->title }}</span>
		<br>
		{!! $page->description !!}
	</div>
	<div class="clear"></div>
</div>
