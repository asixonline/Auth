@extends('front')
<?php

$conf_sitename = \App\CmsConfig::where('alias', 'site_name')->first()->value;

$metas_title = $conf_sitename;
$metas_descr = \App\Util\XMLParser::getValue($page->metas, 'description');
$metas_keywr = \App\Util\XMLParser::getValue($page->metas, 'keywords');
$metas_robot = \App\Util\XMLParser::getValue($page->metas, 'robots');
$metas_image = \App\Util\XMLParser::getValue($page->metas, 'image');
$metas_url	 = url('/');

$parent_0=$page;

$bloque_animacion=$parent_0->child_template('bloque_animacion')->first();
$acceso_video=$parent_0->child_template('acceso_video')->first();
$bloque_widget=$parent_0->child_template('bloque_widget')->first();
$contenedor=$parent_0->child_template('contenedor')->first();
?>
@section('content')
	<section class="wrapper_principal page-home">

	@if($bloque_animacion)
		@include('front.partials.home.bloque_animacion')
	@endif
		<div class="container">

		@if($acceso_video)
			@include('front.partials.home.acceso_video')
		@endif

		@if($bloque_widget)
			@include('front.partials.home.bloque_widget')
		@endif
			<div class="clear"></div>

		@if($contenedor)
		<?php
		$items=$contenedor->children;
		?>
			@foreach($items as $acceso)
				@include('front.partials.home.'.$acceso->schema->front_view)
			@endforeach
		@endif
			<div class="clear"></div>

		</div>

	</section>
@endsection

@section('meta_tag')
	<title>{{ $metas_title }}</title>
	<meta name="description" content="{{ $metas_descr }}" />
	<meta name="keywords" content="{{ $metas_keywr }}" />
	<meta name="robots" content="{{ $metas_robot }}" />
	<meta property="og:title" content="{{ $metas_title }}" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="{{ url('userfiles/'.$metas_image) }}" />
	<meta property="og:url" content="{{ $metas_url }}" /> 
@endsection

@section('header_js')

@endsection
