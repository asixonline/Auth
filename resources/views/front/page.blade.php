@extends('front')
<?php

$conf_sitename = \App\CmsConfig::where('alias', 'site_name')->first()->value;

$metas_title = $page->title.' - '.$conf_sitename;
$metas_descr = \App\Util\XMLParser::getValue($page->metas, 'description');
$metas_keywr = \App\Util\XMLParser::getValue($page->metas, 'keywords');
$metas_robot = \App\Util\XMLParser::getValue($page->metas, 'robots');
$metas_image = \App\Util\XMLParser::getValue($page->metas, 'image');
$metas_url	 = \App\Util\SEO::url_article($page);

$schema=$page->schema;
$front_view = 'front.templates.'.$schema->front_view;

$parent_0=count($parents)>0? $parents[0]: $page;
$bloque_widget = $parent_0->child_template('bloque_widget')->first();
?>
@section('content')
    <section class="wrapper_principal wrapper_interna">

		@include('front.partials.banner_interna')
		@include('front.partials.breadcrumb')
		<div class="container">
			<ul class="sidebar">
				@include('front.partials.submenu')
			</ul>
			@if(View::exists($front_view))
				@include($front_view)
			@else
				@include('front.partials.missing_template')
			@endif
			<div class="clear"></div>
		</div>
		
		@if($bloque_widget and !in_array($schema->front_view, ['pagina_proyecto', 'proyecto']))
			<div class="area_gris">
				@include('front.partials.bloque_widget')
			</div>
		@endif
	</section>
@endsection

@section('meta_tag')
	<title>{{ $metas_title }}</title>
	<meta name="description" content="{{ $metas_descr }}">
	<meta name="keywords" content="{{ $metas_keywr }}">
	<meta name="robots" content="{{ $metas_robot }}" />
	<meta property="og:title" content="{{ $metas_title }}" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="{{ url('userfiles/'.$metas_image) }}" />
	<meta property="og:url" content="{{ $metas_url }}" /> 
@endsection

@section('header_js')

@endsection
