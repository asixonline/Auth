	@foreach($list as $item)
	<?php
		$url=url('/'.$item->slug.'.html');
		$estilo=\App\Util\XMLParser::getValue($page->param, 'estilo');
		$color=\App\Util\XMLParser::getValue($page->param, 'color');
		$punto=\App\Util\XMLParser::getValue($item->param, 'punto');
		$dot=explode(',', $punto);
		$dot_x=count($dot)>1? $dot[0]: 0;
		$dot_y=count($dot)>1? $dot[1]+8: 0;
	?>
	<div class="point" style="left: {{ $dot_x }}px; top: {{ $dot_y }}px">
		<div class="ball rojo">{{ $item->position }}</div>
		<div class="box">
			<a href="{{ $url }}" class="full"></a>
			<div class="camion">
				<img src="{{ asset('/assets/front/images/carreteras/carreteras-camion-btn.svg') }}">
				<div class="num">{{ $item->position }}</div>
			</div>
			<div class="nube azul">
				<a href="{{ $url }}" class="full"></a>
				<div class="titulo">										
					<div class="nombre">
						{{ $item->subtitle }}
					</div>
					<div class="solapa"></div>
				</div>
			</div>
		</div>
	</div>
	@endforeach
