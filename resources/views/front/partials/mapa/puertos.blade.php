	@foreach($list as $item)
	<?php
		$url=url('/'.$item->slug.'.html');
		$estilo=\App\Util\XMLParser::getValue($page->param, 'estilo');
		$color=\App\Util\XMLParser::getValue($item->param, 'color');
		$punto=\App\Util\XMLParser::getValue($item->param, 'punto');
		$dot=explode(',', $punto);
		$dot_x=count($dot)>1? $dot[0]: 0;
		$dot_y=count($dot)>1? $dot[1]+8: 0;
	?>
	<div class="point" style="left: {{ $dot_x }}px; top: {{ $dot_y }}px">
		<div class="ball {{ $color }}">{{ $item->position }}</div>
		<div class="box">
			<a href="{{ $url }}" class="full"></a>
			<div class="barco {{ $color }}">
				<img src="{{ asset('/assets/front/images/puertos/barco.svg') }}">
			</div>
			<div class="nube {{ $color }}">
				<a href="{{ $url }}" class="full"></a>
				<div class="titulo">
					<div class="num {{ $color }}">{{ $item->position }}</div>
					<div class="texto">{{ $item->subtitle }}</div>
					<div class="nombre">{{ $item->title }}</div>
					<div class="solapa"></div>
				</div>
			</div>
		</div>
	</div>
	@endforeach
