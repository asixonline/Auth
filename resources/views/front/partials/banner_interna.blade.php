<?php
$banner=$parent_0;
$imagen=\App\Util\XMLParser::getValue($banner->media, 'imagen');
?>
<div class="banner" style="background: url('{{ asset('/userfiles/'.$imagen) }}') center">
	<div class="esquina">
		<img src="{{ asset('/assets/front/images/header_esq.png') }}">
	</div>
	<div class="container">
	  	<div class="diagonal">
			<div class="d1"></div>
			<div class="d2"></div>
			<div class="d3"></div>
		</div>
		<div class="reloj_box">
			{!! $banner->resumen !!}
		</div>
	</div>
</div>
