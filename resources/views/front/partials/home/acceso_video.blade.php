<?php
$imagen=\App\Util\XMLParser::getValue($acceso_video->media, 'imagen');
$video=\App\Util\XMLParser::getValue($acceso_video->param, 'video');
$url = \App\Util\SEO::url_redirect($acceso_video);
?>
	<div class="home_informate">
		<div class="video_box lista_galeria">
			<a href="{{ $video }}&fs=1&amp;autoplay=1&amp;rel=0" class="fancybox_video" rel="layer_videos">
				<img src="{{ asset('/userfiles/'.$imagen) }}" />
			</a>
		</div>
		<div class="right_box">
			<div class="titulo">
				{{ $acceso_video->title }}
			</div>
			<div class="texto100">
				{!! $acceso_video->resumen !!}
			</div>						
			<div class="ico_mas">
				<a href="{{ $url }}"><img src="{{ asset('/assets/front/images/ico_mas.png') }}"></a>
			</div>
		</div>
		<div class="clear"></div>
	</div>
