<?php
use \App\Util\XMLParser;

$items=$bloque_animacion->children;
?>
	<div class="banner">
		<div class="esquina">
			<img src="{{ asset('/assets/front/images/header_esq.png') }}">
		</div>
		<div class="box_slider">
			<ul class="bxslider">
			@foreach($items as $item)
			<?php
			$imagen=XMLParser::getValue($item->media, 'imagen');
			$background=$imagen!=NULL? 'background: url('.asset('/userfiles/'.$imagen).') no-repeat center': NULL;
			?>
				<li style="{{ $background }}"></li>
			@endforeach
			</ul>
		</div>
		<div class="container">
		  	<div class="diagonal">
				<div class="d1"></div>
				<div class="d2"></div>
				<div class="d3"></div>
			</div>
			<ul class="lista_slogans">
			@foreach($items as $item)
			<?php
				$hide_title=XMLParser::getValue($item->param, 'hide_title');
			?>
				<li>
					<h3>
					@if(!$hide_title)
						{{ $item->title }}
						<span>{{ $item->subtitle }}</span>
					@endif
					</h3>
				</li>
			@endforeach
			</ul>
			<div class="reloj_box reloj_home">
				{!! $bloque_animacion->resumen !!}
			</div>
		</div>
	</div>
