<?php
$icono=\App\Util\XMLParser::getValue($acceso->media, 'icono');
$url = \App\Util\SEO::url_redirect($acceso);
?>
	<div class="home_quienessomos">
		<div class="imagen">
			<img src="{{ asset('/userfiles/'.$icono) }}">
		</div>
		<div class="datos">
			<div class="titulo">
				{{ $acceso->title }}
			</div>
			<div class="texto100">
				{!! $acceso->resumen !!}
			</div>

			<div class="ico_mas">
				<a href="{{ $url }}"><img src="{{ asset('/assets/front/images/ico_mas.png') }}"></a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
