<?php
use \App\Util\SEO;
use \App\Util\XMLParser;

$footer = \App\CmsArticle::whereHas('schemas', function ($query) {
    $query->where('front_view', 'seccion_footer');
})
->whereNull('parent_id')
->first();
?>
<footer>
<div class="container">
@if($footer)
<?php
$buscador=$footer->find_template('pagina_buscador')->first();
$url_buscador=$buscador!=NULL? SEO::url_article($buscador): NULL;
$contenedor=$footer->find_template('contenedor')->first();
$redes=$contenedor!=NULL? $contenedor->children: array();
?>
	<div class="texto">
		{!! $footer->description !!}
	</div>
	<div class="right_box">
    {!! Form::open(['url' => $url_buscador, 'method'=>'GET']) !!}
		@if($buscador!=NULL)
			<input type="text" name="q" placeholder="Buscar">
			<input type="submit" style="display: none;" />
		@endif
		@foreach ($redes as $item)
		<?php
			$icono=XMLParser::getValue($item->media, 'icono');
			$url=SEO::url_redirect($item);
			$target=SEO::url_target($item);
		?>
		<div class="ico">
			<a href="{{ $url }}" target="{{ $target }}">
				<img src="{{ asset('/userfiles/'.$icono) }}">
			</a>
		</div>
		@endforeach
	{!! Form::close() !!}
	</div>
	<div class="clear"></div>
@endif
</div>
</footer>
