<?php
use \App\Util\SEO;
use \App\Util\XMLParser;

$toolbar = \App\CmsArticle::whereHas('schemas', function ($query) {
    $query->where('front_view', 'seccion_toolbar');
})
->whereNull('parent_id')
->first();

$menu_top=\App\CmsArticle::select()
	->where('active', '1')
	->whereNull('parent_id')
	->orderBy('position')->get();
?>
<header>
<div class="container">
	<div class="logo">
		<a href="index.html">
			<img src="{{ asset('/assets/front/images/logo.png') }}">
		</a>					
	</div>
	<div class="barra_superior">
		<a href="index.html">Home</a>
@if($toolbar)
<?php
	$toolbar=$toolbar->children;
?>
	@foreach($toolbar as $item)
		| <a href="{{ SEO::url_redirect($item) }}" target="{{ SEO::url_target($item) }}">{{ $item->title }}</a>
	@endforeach
@endif
	</div>
	<div class="clear"></div>
</div>
<nav>
	<ul class="menu">
	@foreach ($menu_top as $menu)
	<?php
		$show_menu=XMLParser::getValue($menu->param, 'show_menu')=='1';
		$show_page=XMLParser::getValue($menu->param, 'show_page')=='1';
		
		if(!$show_menu) continue;
		$smenus=$menu->submenu;
		$activo=$parent_0->id==$menu->id;
	?>
		<li class="{{ $activo? 'activo': '' }}">
			<div class="menu_padre">{{ $menu->title }}</div>
		@if(count($smenus)>0)
		<?php
			$url = SEO::url_article(!$show_page? $smenus[0]: $menu);
		?>
			<a href="{{ $url }}" class="full"></a>
			<ul class="sub_menu">
			@foreach ($smenus as $smenu)
			<?php
				$items=$smenu->submenu;
				$url = SEO::url_article($smenu);
			?>
				<li class="conlink">
					<div class="menu_hijo">{{ $smenu->title }}</div>
				@if(count($items)>0)
					<ul class="ultra_menu">
					@foreach ($items as $item)
					<?php
						$url=url('/'.$item->slug.'.html');
					?>
						<li>
							<a href="{{ $url }}" class="full"></a>
							{{ $item->title }}
						</li>
					@endforeach
					</ul>
				@else
					<a href="{{ $url }}" class="full"></a>
				@endif
				</li>
			@endforeach
			</ul>
		@else
		<?php
			$url=$show_page? url('/'.$menu->slug.'.html'): '#';
		?>
			<a href="{{ $url }}" class="full"></a>
		@endif
		</li>
	@endforeach
	</ul>
</nav>
</header>
