<?php
use \App\Util\XMLParser;
use \App\Util\SEO;

$items=$bloque_widget->children;
?>
	<div class="container">
		<div class="left_box">
			<div class="titulo">
				{{ $bloque_widget->title }}
			</div>
			<div class="texto100">
				{!! $bloque_widget->resumen !!}
			</div>
		</div>
		<ul class="lista_icono">
		@foreach($items as $item)
		<?php
		$imagen=XMLParser::getValue($item->media, 'imagen');
		$url = SEO::url_redirect($item);
		?>
			<li>
				<a href="{{ $url }}" class="full"></a>
				<img src="{{ asset('/userfiles/'.$imagen) }}">
				<div class="name"> {{ $item->title }}</div>
			</li>
		@endforeach
		</ul>
		<div class="clear"></div>
	</div>
