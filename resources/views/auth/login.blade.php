@extends('login')

@section('content')
    <div class="login-box-body">
        <p class="login-box-msg">Login</p>

		<form role="form" method="POST" action="{{ url('/auth/login') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group has-feedback">
				<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Usuario" autocomplete="off">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div class="row">
				<div class="col-xs-8">
				  <div class="checkbox icheck">
				    <label>
				      <input type="checkbox" name="remember"> Remember Me
				    </label>
				  </div>
				</div>
				<!-- /.col -->
				<div class="col-xs-4">
				  <button type="submit" id="btnSubmit" name="btSubmit" class="btn btn-primary btn-block btn-flat">Sign In</button>
				</div>
			<!-- /.col -->
			</div>
		</form>
        <!-- /.social-auth-links -->
        <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
    </div>
@endsection
