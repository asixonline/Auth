<?php
$conf_analytics = \App\CmsConfig::where('alias', 'analytics')->first()->value;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
@yield('meta_tag')
	<link rel="shortcut icon" href="{{ asset('/assets/favicon.ico') }}" type="image/x-icon" />
	<link rel="icon" href="{{ asset('/assets/favicon.ico') }}" type="image/ico" />
	<!-- Global Javascript & CSS here /-->
	<script src="{{ asset('/assets/front/js/jquery-1.11.3.min.js') }}"></script>
	<!-- Custom Javascript & CSS here /-->
@yield('header_js')
</head>
<body>
	<div id="loader"></div>
  	<div class="wrapper">
		@include('front.partials.header')
		@include('front.partials.error_handler')
		@yield('content')
		@include('front.partials.footer')
	</div>
{!! $conf_analytics !!}
</body>
</html>
