<?php
$video=\App\Util\XMLParser::getValue($article->media, 'video');

$directory=\App\CmsDirectory::select()->where('alias', 'galeria_video')->first()->path;
?>
	<div class="form-group">
	  {!! Form::label('media[video]', 'Imagen', ['class'=>'col-sm-3 col-lg-1 control-label']) !!}
	  <div class="col-sm-9 col-lg-11">
	    <div class="input-group">
	      {!! Form::text('media[video]', $video, ['class'=>'form-control fmanager', 'id'=>'media_video', 'rel'=>$directory ]) !!}
	    </div>
	  </div>
	</div>
