<?php
$imagen=\App\Util\XMLParser::getValue($article->media, 'imagen');
$show_menu=\App\Util\XMLParser::getValue($article->param, 'show_menu');
$show_page=\App\Util\XMLParser::getValue($article->param, 'show_page');

$directory=\App\CmsDirectory::select()->where('alias', 'seccion_imagen')->first()->path;
?>
	<div class="form-group">
	  {!! Form::label('media[imagen]', 'Imagen', ['class'=>'col-sm-3 col-lg-1 control-label']) !!}
	  <div class="col-sm-9 col-lg-11">
	    <div class="input-group">
	      {!! Form::text('media[imagen]', $imagen, ['class'=>'form-control fmanager', 'id'=>'media_imagen', 'rel'=>$directory ]) !!}
	    </div>
	  </div>
	</div>
	<div class="form-group">
	  {!! Form::label('description', 'Descripción', ['class'=>'col-sm-3 col-lg-1 control-label']) !!}
	  <div class="col-sm-9 col-lg-11">
	      {!! Form::textarea('description', null, ['class'=>'form-control ckeditor']) !!}
	  </div>
	</div>
	<div class="form-group">
	  {!! Form::label('', '', ['class'=>'col-sm-3 col-lg-1 control-label']) !!}
		<label class="col-sm-9 col-lg-11">
		  {!! Form::checkbox('param[show_menu]', 1, $show_menu) !!}
			Mostrar en menú
		</label>
	</div>
	<div class="form-group">
	  {!! Form::label('', '', ['class'=>'col-sm-3 col-lg-1 control-label']) !!}
		<label class="col-sm-9 col-lg-11">
		  {!! Form::checkbox('param[show_page]', 1, $show_page) !!}
			Ver como página
		</label>
	</div>
