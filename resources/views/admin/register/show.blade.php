@extends('admin')

@section('content')
<div class="box box-default">
	<div class="box-header">
		<h2 class="box-title"><i class="fa fa-edit"></i> Editar {{ $current_module->title }}: {{$register->name}}</h2><i class="fa fa-close pull-right"  onclick="javascript:history.back();"></i>
	</div>

	<div class="box-body">

		<div class="form-group row">
			<div class="col-sm-3 col-lg-2"><label>Tipo de Solicitud</label></div>
			<div class="col-sm-9 col-lg-10">{{ $register->contact->name }}</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-3 col-lg-2"><label>Nombres</label></div>
			<div class="col-sm-9 col-lg-10">{{ $register->first_name }}</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-3 col-lg-2"><label>Apellidos</label></div>
			<div class="col-sm-9 col-lg-10">{{ $register->last_name }}</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-3 col-lg-2"><label>Teléfono</label></div>
			<div class="col-sm-9 col-lg-10">{{ $register->phone }}</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-3 col-lg-2"><label>DNI</label></div>
			<div class="col-sm-9 col-lg-10">{{ $register->dni }}</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-3 col-lg-2"><label>Dirección</label></div>
			<div class="col-sm-9 col-lg-10">{{ $register->address }}</div>
		</div>
<!--
		<div class="form-group row">
			<div class="col-sm-3 col-lg-2"><label>Ciudad</label></div>
			<div class="col-sm-9 col-lg-10">{{ $register->city }}</div>
		</div>
/-->
		<div class="form-group row">
			<div class="col-sm-3 col-lg-2"><label>Teléfono</label></div>
			<div class="col-sm-9 col-lg-10">{{ $register->phone }}</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-3 col-lg-2"><label>Email</label></div>
			<div class="col-sm-9 col-lg-10">{{ $register->email }}</div>
		</div>

		<div class="form-group row">
			<div class="col-sm-3 col-lg-2"><label>Comentario</label></div>
			<div class="col-sm-9 col-lg-10">{{ $register->comment }}</div>
		</div>

	</div>
	<div class="box-footer">
		<a href="{{ route('admin.register.index') }}{{ $module_params }}" class="btn btn-danger"><span class="fa fa-arrow-left"></span> regresar </a>
	</div>
</div>
@endsection
