<?php

return [
    'oracle' => [
        'driver'        => 'oracle',
        'tns'           => env('DB_TNS', 'localhost/xe'),
        'host'          => env('DB_HOST', 'localhost'),
        'port'          => env('DB_PORT', '1521'),
        'database'      => env('DB_DATABASE', 'OSITRAN'),
        'username'      => env('DB_USERNAME', 'SYSTEM'),
        'password'      => env('DB_PASSWORD', 'asixon'),
        'charset'       => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'        => env('DB_PREFIX', ''),
        'prefix_schema' => env('DB_SCHEMA_PREFIX', ''),
    ],
];
