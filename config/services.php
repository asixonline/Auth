<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => 'ositran.asixonline.com',
		'secret' => '__ozy05BrNu67',
	],

	'mandrill' => [
		'secret' => '__ozy05BrNu67',
	],

	'ses' => [
		'key' => 'ositran',
		'secret' => '__ozy05BrNu67',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\AdmUser',
		'secret' => '__ozy05BrNu67',
	],

];
