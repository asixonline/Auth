<?php

return [
    'entities' => [
        'cms_articles' => '\App\CmsArticle',
        'cms_schemas' => '\App\CmsSchema'
        // or
        // 'articles' => ['entity' => '\Article', 'relation' => 'tags']
    ],
];
