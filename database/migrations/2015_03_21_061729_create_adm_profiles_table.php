<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('adm_profiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 255);
			$table->boolean('active')->nullable();
			$table->boolean('sa')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('adm_users');
		Schema::dropIfExists('adm_profiles');
	}

}
