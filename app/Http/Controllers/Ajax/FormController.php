<?php namespace App\Http\Controllers\Ajax;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Redirector;

use App\CrmForm;
use App\CrmRegister;
use App\CrmNotify;

use DB;
use View;
use Mail;

class FormController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Front Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/
	public function __construct()
	{
		//$this->middleware('guest');

		//date_default_timezone_set('America/Lima');
		//setlocale(LC_TIME, "es_PE");
	}

	public function refereshCaptcha(){
	    return captcha_img('simple');
	}

	public function store()
	{
        $rules = ['captcha' => 'required|captcha'];
        $validator = Validator::make(['captcha'=>Input::get('captcha')], $rules);
        if ($validator->fails())
        {
			return response()->json(['resp' => '0', 'msg' => 'El captcha ingresado es incorrecto.']);
        }

        $rules = ['form_id'=>'required', 'contact_id'=>'required', 'first_name'=>'required', 'last_name'=>'required', 'email'=>'required', 'checkin'=>'required'];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
			return response()->json(['resp' => '0', 'msg' => 'Algunos datos no se han ingresado correctamente. Por favor complételos.']);
        }

        $form=CrmForm::find(Input::get('form_id'));
        if(!$form)
        {
			return response()->json(['resp' => '0', 'msg' => 'El formulario no se encuentra debidamente registrado, por favor intentelo mas tarde.']);
        }

		$register = new CrmRegister;
		$register->form_id = $form->id;
		$register->contact_id = Input::get('contact_id');
		$register->first_name = Input::get('first_name');
		$register->last_name = Input::get('last_name');
		$register->dni = Input::get('dni');
		$register->address = Input::get('address');
		$register->city = Input::get('city');
		$register->phone = Input::get('phone');
		$register->comment = Input::get('comment');
		$register->checkin = Input::get('checkin');
		$register->email = Input::get('email');
		$register->save();

		//Send mail
		$notifies=CrmNotify::select()
			->where('form_id', $form->id)
			->where('active', '1')
			->get();
		
		$from=array();
		$from['name'] = \App\CmsConfig::where('alias', 'site_name')->first()->value;
		$from['email'] = \App\CmsConfig::where('alias', 'postmaster')->first()->value;

		foreach ($notifies as $notify) {
	        Mail::send('emails.contacto', ['register' => $register], function ($m) use ($notify, $from) {
	        	$m->from($from['email'], $from['name']);
	            $m->to($notify->user->email)->subject('Solicitud de Contacto');
	            if(!empty($notify->recipients)) $m->cc(explode(',', $notify->recipients));
	        });
		}

		return response()->json(['resp' => '1', 'msg' => 'Los datos se registraron correctamente']);
	}

}
