<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Redirector;
use App\Http\Requests\Admin\CreateRegisterRequest;
use App\Http\Requests\Admin\UpdateRegisterRequest;
use App\CrmRegister;
use App\CrmForm;

use Maatwebsite\Excel\Facades\Excel;
use View;

class RegisterController extends AdminController {

	public $form;
	public $module_params;

    public function __construct()
    {
		$form_id = Request::input('form_id');
		$this->form = !empty($form_id)? \App\CrmForm::find($form_id): \App\CrmForm::select()->where('active', '1')->first();

		$this->module_params = '?form_id='.$this->form->id;

		View::share('form_id', $this->form->id);
		View::share('module_params', $this->module_params);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$filter = Request::has('filter')? Request::input('filter'): NULL;
		$registers=CrmRegister::select()
			->where('form_id', $this->form->id)
			->where(function($query) use ($filter){
				$query->where('first_name', 'LIKE', '%'.$filter.'%');
				$query->orWhere('last_name', 'LIKE', '%'.$filter.'%');
             });

		if(Request::has('export')){
			return Excel::create('Registro_'.date('dmY'), function($excel) use($registers){
			    $excel->sheet('Lista', function($sheet) use($registers){
			        // Sheet manipulation
					$sheet->fromArray($registers->get()->toArray());
					//$sheet->rows($registers->get()->toArray());
			    });
			})->export('xls');
		}

		$forms=CrmForm::select()->lists('name', 'id');

		View::share('filter', $filter);

        return view('admin.register.index', array('registers'=>$registers->Paginate(), 'forms'=>$forms));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('admin.register.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateRegisterRequest $request)
	{
		$register = CrmRegister::Create($request->all());

		\App\Util\RegisterLog::add($register);
		return redirect('admin/register/'.$this->module_params);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$register = CrmRegister::FindOrFail($id);

        return view('admin.register.show', compact('register'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$register = CrmRegister::FindOrFail($id);

        return view('admin.register.edit', compact('register'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UpdateRegisterRequest $request, $id)
	{
		$register = CrmRegister::FindOrFail($id);
		$register->fill($request->all());
		$register->save();

		\App\Util\RegisterLog::add($register);
		return redirect('admin/register/'.$this->module_params);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$register = CrmRegister::FindOrFail($id);
		$register->delete();

		\App\Util\RegisterLog::add($register);
		return redirect('admin/register/'.$this->module_params);
	}

}
