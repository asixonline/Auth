<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;

use \App\Util\SEO;

use DB;
use View;

class FrontController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Front Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/
	public function __construct()
	{
		//$this->middleware('guest');

		//date_default_timezone_set('America/Lima');
		//setlocale(LC_TIME, "es_PE");
	}

	public function index()
	{
		$page = \App\CmsArticle::whereHas('schemas', function ($query) {
		    $query->where('front_view', 'seccion_home');
		})
		->whereNull('parent_id')
		->first();

		//Page not found
		if(!$page){
			return view('front.maintenance');
		}
		
		$parents=array();

		return view('front.home', array('page'=>$page, 'parents'=>$parents));
	}

	public function page($slug)
	{
		$slug=str_replace('.html', '', $slug);
		$page=\App\CmsArticle::select()->where('slug', $slug)->first();

		//Page not found
		if(!$page){
			$url=SEO::url_notfound();
			return redirect($url);
		}

		//Redirect to parent
		if(!$page->schema->is_page){
			$url=SEO::url_redirect_id($page->parent_id);
			return redirect($url);
		}

		$parent=\App\CmsArticle::find($page->parent_id);
		$parents=array();

		while ($parent!=NULL)
		{
			$parents[]=$parent;
			$parent=\App\CmsArticle::find($parent->parent_id);
		}

		return view('front.page', array('page'=>$page, 'parents'=>array_reverse($parents)));
	}

}
