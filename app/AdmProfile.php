<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmProfile extends Model {

	protected $table = 'adm_profiles';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'active'];

}
