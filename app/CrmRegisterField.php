<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CrmRegisterField extends Model {

	protected $table = 'crm_register_fields';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['register_id', 'field_id', 'value', 'txt_value'];

}
